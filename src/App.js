import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import AppContainer from './components/AppContainer';


function App() {
  return (
    <AppContainer />
  );
}

export default App;
