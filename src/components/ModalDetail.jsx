import React, {Component} from 'react'
import {Button, Header, Icon, Modal} from 'semantic-ui-react'
import {capitalizeLetter} from '../common/helper';

export default class ModalDetail extends Component {
  state = {
    modalOpen: this.props.shown,
    row: this.props.row,
    modalData: {},
  }

  getDetailNormalizedUrl = (id) => {
    let {url} = this.props;
    url = url.replace('{{user}}', id);
    return url;
  }

  handleOpen = async () => {
    const {userId} = this.state.row;

    let response = await fetch(this.getDetailNormalizedUrl(userId));
    let json = await response.json();

    this.setState({modalData: json}, () => {
      this.setState({modalOpen: !this.state.modalOpen});
    })
  }
  handleClose = () => this.setState({modalOpen: false})

  render() {
    const {userId, login, password, title, lastName, firstName, gender, email, picture, address} = this.state.modalData;

    return (
      <Modal
        trigger={<Button onClick={this.handleOpen}>Show Modal</Button>}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        basic
      >
        <Header icon='browser' content={`User #${userId}`}/>
        <Modal.Content>
          <h3>{`${title}. ${capitalizeLetter(firstName)} ${capitalizeLetter(lastName)}`}</h3>
          <img src={picture} alt="..."/>
          <p>Gender: {gender}</p>
          <p>Email: {email}</p>
          <p>Address: {address}</p>
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' onClick={this.handleClose} inverted>
            <Icon name='checkmark'/> Got it
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
