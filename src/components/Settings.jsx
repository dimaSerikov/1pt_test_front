import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';

export default class Settings extends Component {
  render(){
    const {values} = this.props;

    return(
      <Form color='blue'>
        <h1 className="ui centered">Enter Personal Details</h1>
        <Form.Field>
          <label>URL for all users</label>
          <input placeholder='http://hostname/users'
                 onChange={e => this.props.handleChange('listUrl', e)}
                 defaultValue={values.listUrl}
          />
        </Form.Field>
        <Form.Field>
          <label>URL for one user</label>
          <input placeholder='http://hostname/user/id'
                 onChange={e => this.props.handleChange('entityUrl', e)}
                 defaultValue={values.entityUrl}
          />
        </Form.Field>

        <Form.Field>
          <label>Entity ID</label>
          <input placeholder='userId'
                 onChange={e => this.props.handleChange('entityId', e)}
                 defaultValue={values.entityId}
          />
        </Form.Field>
        <Form.Field>
          <label>Rows limit</label>
          <input placeholder='100'
                 onChange={e => this.props.handleChange('limit', e)}
                 defaultValue={values.limit}
          />
        </Form.Field>
        <Form.Field>
          <label>Page size</label>
          <input placeholder='10'
                 onChange={e => this.props.handleChange('pageSize', e)}
                 defaultValue={values.pageSize}
          />
        </Form.Field>
        <Form.Field>
          <label>Rows offset</label>
          <input placeholder='0'
                 onChange={e => this.props.handleChange('offset', e)}
                 defaultValue={values.offset}
          />
        </Form.Field>

        <Button onClick={e => this.props.handleMenuSwitch('client')}>Go to Client page</Button>
      </Form>
    )
  }
}
