import React, {Component} from 'react';
import Settings from './Settings';
import Client from './Client';
import Debug from './Debug';

export default class MainForm extends Component {

  render() {
    const {page, values} = this.props;

    switch (page) {
      case 'debug':
        return <Debug
          handleChange={this.props.handleChange}
          handleMenuSwitch={this.props.handleMenuSwitch}
          values={values}
        />;
      case 'client':
        return <Client
          handleChange={this.props.handleChange}
          handleMenuSwitch={this.props.handleMenuSwitch}
          values={values}
        />;
      case 'settings':
      default:
        return <Settings
          handleChange={this.props.handleChange}
          handleMenuSwitch={this.props.handleMenuSwitch}
          values={values}
        />;
    }
  }
}
