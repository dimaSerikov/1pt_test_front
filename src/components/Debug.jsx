import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';

export default class Debug extends Component {

  state = {
    link: this.props.values.listUrl,
    requestResult: '',
  };

  handleChange = (e) => {
    e.preventDefault();

    this.setState({link: e.target.value})
  }

  sendRequest = async () => {
    let response = await fetch(this.getNormalizedUrl(this.state.link));
    let json = await response.json();

    this.setState({requestResult: JSON.stringify(json, undefined, 4)})
  }

  getNormalizedUrl = (url) => {
    if (!url.includes('{{')) {
      return url;
    }
    const {values} = this.props;
    const {limit, offset} = values;

    const hrefObj = new URL(url);
    const searchParams = new URLSearchParams(hrefObj.search);

    for(let value of searchParams.values()) {
      if (value === '{{limit}}') {
        hrefObj.searchParams.set('limit', limit);
      } else if (value === '{{offset}}') {
        hrefObj.searchParams.set('offset', offset);
      } else if (value === '{{filter}}') {
        hrefObj.searchParams.set('name', '');
      } else {
        hrefObj.searchParams.set(value, '');
      }
    }

    return hrefObj.href;
  }

  render() {
    return(
      <Form color='blue'>
        <h1 className="ui centered">Debugger</h1>
        <Form.Field>
          <label>URL to resquest</label>
          <input placeholder='http://hostname/users'
                 defaultValue={this.state.link}
                 onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <label>URL for one user</label>
          <textarea defaultValue={this.state.requestResult} />
        </Form.Field>

        <Button onClick={this.sendRequest}>Request</Button>
      </Form>
    )
  }
}
