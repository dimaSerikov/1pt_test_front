import React, {Component} from 'react';
import {Container, Image, Menu} from 'semantic-ui-react';

export default class NavMenu extends Component {

  handleSwitch(name) {
    this.props.handleMenuSwitch(name);
  }

  render() {
    return (
      <Menu fixed='top' inverted>
        <Container>
          <Menu.Item as='a' header>
            <Image size='mini' src='/logo192.png' style={{ marginRight: '1.5em' }} />
            Practical Exercise Client
          </Menu.Item>
          <Menu.Item as='a' onClick={e => this.handleSwitch('settings')}>Settings</Menu.Item>
          <Menu.Item as='a' onClick={e => this.handleSwitch('client')}>Client</Menu.Item>
          <Menu.Item as='a' onClick={e => this.handleSwitch('debug')}>Debug</Menu.Item>
        </Container>
      </Menu>
    );
  }
}
