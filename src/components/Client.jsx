import React, { Component } from 'react';
import ModalDetail from './ModalDetail';
import DataTable from 'react-data-table-component';

export default class Client extends Component {
  state = {
    testTakers: [],
    showModal: false,
    modalData: {},
  };

  componentDidMount() {
    this.loadEntityes();
  }

  getListNormalizedUrl = () => {
    const {values} = this.props;
    const {listUrl, limit, offset} = values;

    const hrefObj = new URL(listUrl);
    const searchParams = new URLSearchParams(hrefObj.search);

    for(let value of searchParams.values()) {
      if (value === '{{limit}}') {
        hrefObj.searchParams.set('limit', limit);
      } else if (value === '{{offset}}') {
        hrefObj.searchParams.set('offset', offset);
      } else if (value === '{{filter}}') {
        hrefObj.searchParams.set('name', '');
      } else {
        hrefObj.searchParams.set(value, '');
      }
    }

    return hrefObj.href;
  }

  loadEntityes = async () => {
    const url = this.getListNormalizedUrl();

    let response = await fetch(url);
    let testTakers = await response.json();

    this.setState({testTakers: testTakers})
  }

  render() {
    const {values} = this.props;
    const {pageSize, entityUrl} = values;

    const testTakers = this.state.testTakers;
    const paginationOptions = { paginationPerPage: pageSize };
    const columns = [
      {
        name: 'First Name',
        selector: 'firstName',
        sortable: true,
      },
      {
        name: 'Last Name',
        selector: 'lastName',
        sortable: true,
      },
      {
        name: '',
        selector: '',
        button: true,
        cell: row => <ModalDetail shown={this.state.showModal} row={row} url={entityUrl}>Detail</ModalDetail>,
      }
    ];

    return (
      <DataTable
        title="Test takers list"
        columns={columns}
        data={testTakers}
        pagination
        paginationComponentOptions={paginationOptions}
      />
    )
  }
}
