import React, {Component} from 'react';
import {Container} from 'semantic-ui-react';
import MainForm from './MainForm';
import NavMenu from './NavMenu';

export default class AppContainer extends Component {
  state = {
    page: 'settings',
    listUrl: 'https://hr.oat.taocloud.org/v1/users?limit={{limit}}&offset={{offset}}&name={{filter}}',
    entityUrl: 'https://hr.oat.taocloud.org/v1/user/{{user}}',
    entityId: 'userId',
    limit: 100,
    pageSize: 10,
    offset: 0,
    filter: '',
  };

  handleMenuSwitch = (name) => {
    const {page, listUrl, entityUrl} = this.state;

    if (page === name) {
      return;
    }
    if (!listUrl || !entityUrl) {
      alert('You have to paste urls!');
      return;
    }
    this.setState({page: name})
  };

  handleChange = (input, event) => {
    this.setState({[input]: event.target.value});
  }

  render() {
    const {page} = this.state;
    const {listUrl, entityUrl, entityId, limit, pageSize, offset, testTakers} = this.state;
    const values = {listUrl, entityUrl, entityId, limit, pageSize, offset, testTakers};

    return (
      <>
        <NavMenu handleMenuSwitch={this.handleMenuSwitch} />
        <Container style={{ marginTop: '7em' }}>
          <MainForm
            handleChange={this.handleChange}
            values={values}
            handleMenuSwitch={this.handleMenuSwitch}
            page={page}
          />
        </Container>
      </>
    );
  }
}
